package kaffee.json.objects;

import java.util.List;

public class BankAccount {

	private String accountId;
	private String accountNumber;
	private double balance;
	private String bankCode;
	private String bankLoginId;
	private String bic;
	private double creditLine;
	private String currency;
	private String iban;
	private String icon;
	private double monthlySpendingLimit;
	private String name;
	private String owner;

	private List<BankTransaction> transactions;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankLoginId() {
		return bankLoginId;
	}

	public void setBankLoginId(String bankLoginId) {
		this.bankLoginId = bankLoginId;
	}

	public String getBIC() {
		return bic;
	}

	public void setBIC(String bIC) {
		bic = bIC;
	}

	public double getCreditLine() {
		return creditLine;
	}

	public void setCreditLine(double creditLine) {
		this.creditLine = creditLine;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public double getMonthlySpendingLimit() {
		return monthlySpendingLimit;
	}

	public void setMonthlySpendingLimit(double monthlySpendingLimit) {
		this.monthlySpendingLimit = monthlySpendingLimit;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public List<BankTransaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<BankTransaction> transactions) {
		this.transactions = transactions;
	}

	@Override
	public String toString() {
		return "BankAccount [accountId=" + accountId + ", accountNumber=" + accountNumber + ", balance=" + balance
				+ ", bankCode=" + bankCode + ", bankLoginId=" + bankLoginId + ", BIC=" + bic + ", creditLine="
				+ creditLine + ", currency=" + currency + ", iban=" + iban + ", icon=" + icon
				+ ", monthlySpendingLimit=" + monthlySpendingLimit + ", name=" + name + ", owner=" + owner
				+ ", transactions=" + transactions + "]";
	}

}
