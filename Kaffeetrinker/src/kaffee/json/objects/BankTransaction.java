package kaffee.json.objects;

import java.util.List;

public class BankTransaction {

	private String accountId;
	private double amount;
	private String bookingText;
	private String creditorId;
	private String currency;
	private String dateOfTransaction;
	private String mref;

	private PaymentPartner paymentPartner;

	private String purpose;
	private String sepaPurposeCode;
	private int transactionCode;
	private String transactionId;
	private String typeOfDebit;
	private String typeOfTransactions;
	private List<String> tags;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getBookingText() {
		return bookingText;
	}

	public void setBookingText(String bookingText) {
		this.bookingText = bookingText;
	}

	public String getCreditorId() {
		return creditorId;
	}

	public void setCreditorId(String creditorId) {
		this.creditorId = creditorId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDateOfTransaction() {
		return dateOfTransaction;
	}

	public void setDateOfTransaction(String dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}

	public String getMref() {
		return mref;
	}

	public void setMref(String mref) {
		this.mref = mref;
	}

	public PaymentPartner getPaymentPartner() {
		return paymentPartner;
	}

	public void setPaymentPartner(PaymentPartner partner) {
		this.paymentPartner = partner;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getSepaPurposeCode() {
		return sepaPurposeCode;
	}

	public void setSepaPurposeCode(String sepaPurposeCode) {
		this.sepaPurposeCode = sepaPurposeCode;
	}

	public int getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(int transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTypeOfDebit() {
		return typeOfDebit;
	}

	public void setTypeOfDebit(String typeOfDebit) {
		this.typeOfDebit = typeOfDebit;
	}

	public String getTypeOfTransactions() {
		return typeOfTransactions;
	}

	public void setTypeOfTransactions(String typeOfTransactions) {
		this.typeOfTransactions = typeOfTransactions;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "BankTransaction [accountId=" + accountId + ", amount=" + amount + ", bookingText=" + bookingText
				+ ", creditorId=" + creditorId + ", currency=" + currency + ", dateOfTransaction=" + dateOfTransaction
				+ ", mref=" + mref + ", paymentPartner=" + paymentPartner.toString() + ", purpose=" + purpose
				+ ", sepaPurposeCode=" + sepaPurposeCode + ", transactionCode=" + transactionCode + ", transactionId="
				+ transactionId + ", typeOfDebit=" + typeOfDebit + ", typeOfTransactions=" + typeOfTransactions
				+ ", tags=" + tags + "]";
	}

	public class PaymentPartner {

		private String bic;
		private String creditorId;
		private String iban;
		private String name;

		public String getBIC() {
			return bic;
		}

		public void setBIC(String bIC) {
			bic = bIC;
		}

		public String getCreditorId() {
			return creditorId;
		}

		public void setCreditorId(String creditorId) {
			this.creditorId = creditorId;
		}

		public String getIBAN() {
			return iban;
		}

		public void setIBAN(String iBAN) {
			iban = iBAN;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return "PaymentPartner [bic=" + bic + ", creditorId=" + creditorId + ", iban=" + iban + ", name=" + name
					+ "]";
		}

	}
}
