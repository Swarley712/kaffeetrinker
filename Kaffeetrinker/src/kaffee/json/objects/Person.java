package kaffee.json.objects;

public class Person {

	private String email;

	private String password;

	private String accessToken;

	private String refreshToken;

	public Person(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	@Override
	public String toString() {
		return "email=" + this.getEmail() + " password=" + this.getPassword() + " accessToken=" + this.getAccessToken()
				+ " refreshToken=" + this.getRefreshToken();
	}
}
