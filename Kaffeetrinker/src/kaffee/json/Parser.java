package kaffee.json;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import kaffee.json.objects.BankAccount;
import kaffee.json.objects.BankTransaction;
import kaffee.json.objects.Person;
import kaffee.utils.Utils;

public class Parser {

	private Parser() {
	}

	public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

	public static final JsonParser PARSER = new JsonParser();

	public static Person createUser(String email, String passwd) throws IOException {
		OkHttpClient client = new OkHttpClient();

//		Person pers = new Person("maxmuster@huk-coburg.de", "Maxmuster!");
		Person pers = new Person(email, passwd);

		StringBuilder json = new StringBuilder(new Gson().toJson(pers));

		json.insert(json.indexOf(pers.getPassword() + "\"") + pers.getPassword().length() + 1,
				",\"repeatPassword\":\"" + pers.getPassword() + "\"");

		System.out.println(json.toString());

		RequestBody reqBody = RequestBody.create(JSON, json.toString());

		Request req = new Request.Builder().post(reqBody).url("https://2face.test.fino.cloud/api/register").build();

		Response resp = client.newCall(req).execute();

		JsonElement element = PARSER.parse(resp.body().string());

		JsonElement data = element.getAsJsonObject().get("data");

		pers.setAccessToken(data.getAsJsonObject().get("accessToken").getAsString());
		pers.setAccessToken(data.getAsJsonObject().get("refreshToken").getAsString());

		return pers;
	}

	public static void connectToBank(Person pers) throws IOException {
		OkHttpClient client = new OkHttpClient();

		RequestBody reqBody = RequestBody.create(JSON,
				"{\"bankCode\":\"10020000\",\"username\":\"coderush-melina\",\"secret\":\"coderush-melina1\",\"extraSecret\":null,\"saveSecret\":true}");
//	"{\"bankCode\":\"90090042\",\"username\":\"demo\",\"secret\":\"demo\",\"extraSecret\":null,\"saveSecret\":true}");

		Request req = new Request.Builder().post(reqBody).addHeader("Authorization", "Bearer " + pers.getAccessToken())
				.url("https://2face.test.fino.cloud/api/user/connector/bank/account").build();

		Response resp = client.newCall(req).execute();

		System.out.println(resp.body().string());
	}

	/**
	 * Baut {@link Person} aus email <code>demo@figo.me</code> und Passwort
	 * <code>Demo#1234</code>
	 * 
	 * @return Person mit AccessToken und RefreshToken
	 * @throws IOException
	 */
	public static Person parseJSonUser(String email, String passwd) throws IOException {
		OkHttpClient client = new OkHttpClient();

		Person pers = new Person(email, passwd);

		RequestBody reqBody = RequestBody.create(JSON, new Gson().toJson(pers));

		System.out.println(new Gson().toJson(pers));

		Request req = new Request.Builder().post(reqBody).url("https://2face.test.fino.cloud/api/login").build();

		Response resp = client.newCall(req).execute();

		JsonElement element = PARSER.parse(resp.body().string());

		JsonElement jsonData = element.getAsJsonObject().get("data");

		pers.setAccessToken(jsonData.getAsJsonObject().get("accessToken").getAsString());
		pers.setRefreshToken(jsonData.getAsJsonObject().get("refreshToken").getAsString());

		return pers;
	}

	public static BankAccount createBankAccountFromJson(JsonElement account) {

		return new Gson().fromJson(account, BankAccount.class);
	}

	/**
	 * Erstellt einen Bankaccount aus Person
	 * 
	 * @param person Existierende {@link Person}, die von
	 *               <code>parseJSonUser()</code> erstellt wurde.
	 * @return {@link BankAccount}
	 * @throws IOException
	 * @see {@link #parseJSonUser()}{ @link #getBankAccountJson(Person)}
	 */
	public static BankAccount parseBankAccount(Person person) throws IOException {

		JsonElement oneAccount = getBankAccountJson(person);
		return new Gson().fromJson(oneAccount, BankAccount.class);
	}

	public static List<BankTransaction> filterWohnung(BankAccount account) {
		List<BankTransaction> transaktions = new ArrayList<>();
		LocalDate now = LocalDate.now();
		LocalDate start = now.withDayOfMonth(1);
		LocalDate end = now.withDayOfMonth(now.lengthOfMonth());

		boolean add = false;

		for (BankTransaction transaktion : account.getTransactions()) {
			add = false;
			Date transdate = Utils.getFromUNIX(Integer.parseInt(transaktion.getDateOfTransaction()));
			for (String tag : transaktion.getTags()) {
				if (tag.equals("rental")) {
					add = true;
				}
				if (tag.equals("energy")) {
					add = true;
				}
				if (tag.equals("insurance")) {
					if (transaktion.getPurpose().contains("Hausrat")
							|| transaktion.getPurpose().contains("Wohngeb�ude")) {
						add = true;
					}
				}
				if (transaktion.getPaymentPartner().getName().contains("Rundfunk")) {
					add = true;
				}

				if (add) {
					transaktions.add(transaktion);
				}
			}
		}
		return transaktions;
	}

	public static double earnings(BankAccount account) {
		double gesamtEinkommen = 0;
		boolean add = false;
		for (BankTransaction transaktion : account.getTransactions()) {
			add = false;
			for (String tag : transaktion.getTags()) {
				if (tag.equals("earning")) {
					add = true;
				}
				if (add) {
					gesamtEinkommen += transaktion.getAmount();
				}
			}
		}
		return gesamtEinkommen;
	}

	/**
	 * Gibt wie {@link #parseBankAccount(Person)} Bankaccount zur�ck, aber als
	 * <code>JsonElement</code>
	 * 
	 * @param person existierende person aus {@link #parseJSonUser()}
	 * @return {@link JsonElement} mit Bankaccount Info
	 * @throws IOException
	 */
	public static JsonElement getBankAccountJson(Person person) throws IOException {
		OkHttpClient client = new OkHttpClient();

		Request req = new Request.Builder().get().addHeader("Authorization", "Bearer " + person.getAccessToken())
				.url("https://2face.test.fino.cloud/api/user/connector/bank/account").build();

		Response resp = client.newCall(req).execute();

		String respBody = resp.body().string();

		JsonElement element = PARSER.parse(respBody);

		System.out.println(element);

		JsonElement data = element.getAsJsonObject().get("data");

		return data.getAsJsonObject().get("accounts").getAsJsonArray().get(0);
	}

	public static List<BankTransaction> filterFreizeit(BankAccount bAcc) {

		List<BankTransaction> result = new ArrayList<>();

		List<String> tagsForFreizeit = new ArrayList<>();
		tagsForFreizeit.add("membership_fee");
		tagsForFreizeit.add("abbonement");
		tagsForFreizeit.add("gaming");
		tagsForFreizeit.add("Vergn�gung");

		for (BankTransaction transaction : bAcc.getTransactions()) {
			for (String tag : transaction.getTags()) {
				if (tagsForFreizeit.contains(tag)) {
					result.add(transaction);
				}
			}
		}
		return result;
	}

}
