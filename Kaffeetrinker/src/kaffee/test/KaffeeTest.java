package kaffee.test;

import java.io.IOException;

import kaffee.getter.BankGetter;
import spark.Spark;
import spark.Spark;

public class KaffeeTest {

	public static void main(String[] args) throws IOException {
		BankGetter bGetter = new BankGetter();
		Spark.before((request, response) -> response.type("application/json"));
		Spark.get("/bankAcc", (req, res) -> bGetter.getBankAccount());

//		Person user = Parser.parseJSonUser();
//		BankAccount account = Parser.parseBankAccount(user);
//		List<BankTransaction> wohnungTransaktion = Parser.filterWohnung(account);
//		
//		new FileWriter(
//				new File("C:\\Users\\Felix\\git\\kaffeetrinker\\Kaffeetrinker\\src\\kaffee\\json\\bankaccount.json"))
//						.write(bGetter.getBankAccount().toString());
	}
}