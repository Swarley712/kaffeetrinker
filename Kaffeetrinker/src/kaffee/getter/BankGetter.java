package kaffee.getter;

import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import kaffee.json.Parser;
import kaffee.json.objects.BankAccount;
import kaffee.json.objects.BankTransaction;
import kaffee.json.objects.Person;

public class BankGetter {

	BankAccount account = null;

	public BankGetter() {
	}

	public double getEarnings() {
		return Parser.earnings(account);
	}

	public Person createUser(String email, String passwd) {
		try {
			Person user = Parser.createUser(email, passwd);
			return user;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Person getPerson(String mail, String pw) throws IOException {
		Person user = Parser.parseJSonUser(mail, pw);

		System.out.println(user);

		return user;
	}

	public JsonElement getBankAccount(Person person) {
		try {
			this.account = Parser.parseBankAccount(person);
			return Parser.getBankAccountJson(person);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public JsonElement getFilter(String filter) {
		Gson gson = new Gson();
		JsonArray result = null;
		if (filter.equals("Wohnung")) {
			result = (JsonArray) gson.toJsonTree(Parser.filterWohnung(account), new TypeToken<List<BankTransaction>>() {
			}.getType());

		} else if (filter.equals("Freizeit")) {
			result = (JsonArray) gson.toJsonTree(Parser.filterFreizeit(account),
					new TypeToken<List<BankTransaction>>() {
					}.getType());
		}
		System.out.println(gson.toJson(result));
		return result;
	}

	public void connectToBank(Person pers) {
		try {
			Parser.connectToBank(pers);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}