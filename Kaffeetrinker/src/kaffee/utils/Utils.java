package kaffee.utils;

import java.time.Instant;
import java.util.Date;

public class Utils {

	public static Date getFromUNIX(int unixTimeStamp) {
		return Date.from(Instant.ofEpochSecond(unixTimeStamp));
	}
}
